import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main1 {

	static List<String> studentList = new ArrayList<>();
	static ArrayList<String> l1 = new ArrayList<>();
	static int optionNumber = 0;

	public static void main(String args[]) throws IOException {
		System.out.println("******Welcome To Digital Library****");
		Scanner sc = new Scanner(System.in);
		optionNumber = 0;
		loadBookToList();
		loadStudentToList1();
		login();
		while (optionNumber != 10) {
			optionNumber = 0;
			showOptions();
			System.out.println("Enter the option number:\n\n");

			optionNumber = sc.nextInt();

			if (optionNumber == 1) {
				// login();

			} else if (optionNumber == 2) {
				addBook();

			} else if (optionNumber == 3) {
				viewBookInformation();

			} else if (optionNumber == 4) {
				searchBook();

			} else if (optionNumber == 5) {
				updateBook();

			} else if (optionNumber == 6) {
				createStudentRecord();

			} else if (optionNumber == 7) {
				displayStudentRecord();

			} else if (optionNumber == 8) {
				searchStudentName();

			} else if (optionNumber == 9) {
				updateStudentToRecord();

			} else if (optionNumber == 10) {
				writeArrayListToFile();
				writeArrayListToFile1();
				System.exit(0);

			} else {
				System.out.println("*****Invalid Option****");
			}
		}
	}

	public static void showOptions() {
		System.out.println("Enter your Choise");
		System.out.println("\t1 Login ");
		System.out.println("\t2 Add Book");
		System.out.println("\t3 View Book");
		System.out.println("\t4 Search Book");
		System.out.println("\t5 Update Book");
		System.out.println("\t6 Create Student Record");
		System.out.println("\t7 Display Student Record");
		System.out.println("\t8 Search Student Record");
		System.out.println("\t9 Update Student Record");
		System.out.println("\10 Exit");

	}

	public static void login() {

		System.out.println("**** User Login *****");
		Scanner sc = new Scanner(System.in);
		System.out.println("User Id:");
		int userId = sc.nextInt();
		System.out.println("User Password:");
		int userPassword1 = sc.nextInt();
		if (userId == 123 && userPassword1 == 1234) {
			System.out.println("Login Successful");
		} else if (userId != 123 && userPassword1 != 678) {
			System.out.println(" Login unsuccessful");
			System.out.println("Reenter the User id and password******");
			System.out.println("User Id:");
			int userId1 = sc.nextInt();
			System.out.println("User Password:");
			int userPassword2 = sc.nextInt();
		}

	}

	public static void addBook() {
		System.out.println("******Add Book\n\n");
		Scanner sc = new Scanner(System.in);
		System.out.println(" Enter Book Name:");
		String bookName = sc.nextLine();
		System.out.println("Enter author Name:");
		String authorName = sc.nextLine();
		System.out.println("Publication:");
		String publication = sc.nextLine();
		System.out.println("Price:");
		int price = sc.nextInt();
		l1.add(bookName + "\t\t" + authorName + "\t\t" + publication + "\t\t" + price + "\n");
	}

	public static void viewBookInformation() throws IOException {
		System.out.println("****View Book***");
		String path = "C:\\Users\\Gaurav\\javaWorkspace\\LibraryManagementSystemMiniProject\\src\\LibraryBooks";
		File file = new File(path);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		l1.add("C:\\\\Users\\\\Gaurav\\\\javaWorkspace\\\\LibraryManagementSystemMiniProject\\\\src\\\\LibraryBooks");

	}

	public static void searchBook() {
		System.out.println("****Search Book****");
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter book name you want to Search:");
		String searchBook = sc.nextLine();
		l1.get(getIndexOf(searchBook));
		System.out.println("Your book is present in library" + searchBook);

	}

	public static void updateBook() {
		System.out.println("******Update Book*******");
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter book you want to update:");
		String updateBook = sc.nextLine();
		System.out.println("Book Name:");
		String name = sc.nextLine();
		System.out.println("Book Author Name:");
		String author = sc.nextLine();
		System.out.println("Book Publication:");
		String publication = sc.nextLine();
		System.out.println("Book Price:");
		int price = sc.nextInt();

		l1.set(getIndexOf(updateBook), name + "," + author + "," + publication + "," + price);
	}

	public static int getIndexOf(String bookName) {
		int i = 0;
		for (String s : l1) {
			if (s.contains(bookName)) {
				return i;
			}
			i = i + 1;
		}
		return -1;
	}

	public static void writeArrayListToFile() throws IOException {
		File file = new File("C:\\Users\\Gaurav\\javaWorkspace\\LibraryManagementSystemMiniProject\\src\\LibraryBooks");
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		for (String s : l1) {
			bw.write(s + "\n");
		}
		bw.close();
		fw.close();
	}

	public static void loadBookToList() throws IOException {
		String path = "C:\\\\Users\\\\Gaurav\\\\javaWorkspace\\\\LibraryManagementSystemMiniProject\\\\src\\\\LibraryBooks";
		File file = new File(path);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		while ((line = br.readLine()) != null) {
			l1.add(line);
		}
	}

	public static void createStudentRecord() throws IOException {
		System.out.println("******Student Record*******");
		Scanner sc = new Scanner(System.in);
		System.out.println(" Enter Student Name:");
		String studentName = sc.nextLine();
		System.out.println("Enter Student Branch Name:");
		String branch = sc.nextLine();
		System.out.println("Roll number:");
		int roll = sc.nextInt();

		String path = "C:\\Users\\Gaurav\\javaWorkspace\\LibraryManagementSystemMiniProject\\src\\studentRecords";
		File file = new File(path);
		FileWriter fw = new FileWriter(file, true);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(studentName + "," + branch + "," + roll + "\n");
		bw.close();
		fw.close();
	}

	public static void displayStudentRecord() throws IOException {
		System.out.println("****Display Student Record***");
		String path = "C:\\Users\\Gaurav\\javaWorkspace\\LibraryManagementSystemMiniProject\\src\\studentRecords";
		File file = new File(path);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
	}

	public static int getIndexOfStudentRecord(String studentName) {
		int i = 0;
		for (String s : studentList) {
			if (s.contains(studentName)) {
				return i;
			}
			i = i + 1;
		}
		return -1;
	}

	public static void writeArrayListToFile1() throws IOException {
		File file = new File(
				"C:\\Users\\Gaurav\\javaWorkspace\\LibraryManagementSystemMiniProject\\src\\studentRecords");
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		for (String s : studentList) {
			bw.write(s + "\n");
		}
		bw.close();
		fw.close();
	}

	public static void loadStudentToList1() throws IOException {
		String path = "C:\\Users\\Gaurav\\javaWorkspace\\LibraryManagementSystemMiniProject\\src\\studentRecords";
		File file = new File(path);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		while ((line = br.readLine()) != null) {
			studentList.add(line);
		}
	}

	public static void searchStudentName() {
		System.out.println("****Search Student\n\n");
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter student name you want to search:");
		String studentTOSearch = sc.nextLine();
		studentList.get(getIndexOfStudentRecord(studentTOSearch));
		System.out.println("Student Name Found in student record " + studentTOSearch);
	}

	public static void updateStudentToRecord() {
		System.out.println("*****Update Student*******\n\n");
		System.out.println("Enter Student Name you want to update:/n/n");
		Scanner sc = new Scanner(System.in);
		String studentNameToUpdate = sc.nextLine();
		System.out.println("your updating student :" + studentNameToUpdate);
		System.out.println("Student name:");
		String studentName = sc.nextLine();
		System.out.println("branch:");
		String branch = sc.nextLine();
		System.out.println("Roll no:");
		int roll = sc.nextInt();

		studentList.set(getIndexOfStudentRecord(studentNameToUpdate), studentName + "," + branch + "," + roll + "\n");

	}

}
